# Tomcat Docker Environment #

## Requirements ##
- docker
- docker-compose

## How to start this container up? ##
`sudo docker-compose up`
or, to run in the background in daemon mode:
`sudo docker-compose up -d`

Then access via a web browser on 127.0.0.1/localhost. If port 80 is in use by another command, you may wish to change the container's port-forwarding in the docker-compose.yml file.
